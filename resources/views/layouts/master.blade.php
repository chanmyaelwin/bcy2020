<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>BCY2020</title>

    <link rel="stylesheet" href="/css/app.css">

    <!-- Font Awesome Icons -->
    <!-- <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css"> -->
    <!-- IonIcons -->
    <!-- <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Theme style -->
    <!-- <link rel="stylesheet" href="dist/css/adminlte.min.css"> -->
    <!-- Google Font: Source Sans Pro -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> -->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">
    <!-- Navbar -->
    <!-- <nav class="main-header navbar navbar-expand navbar-white navbar-light"> -->
       <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>

        </ul>

        <!-- SEARCH FORM -->
        <!-- <form class="form-inline ml-3">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form> -->


    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
            <img src="/img/tent.png" alt="BCY 2020 Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">2020</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex ">
                <div class="image ">
                    <img src="/img/user.png" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                <router-link to="/profile" class="d-block">{{Auth::user()->name}}   </router-link>
                </div>

            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <router-link to="/dashboard" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Dashboard
                                <!-- <span class="right badge badge-danger">New</span> -->
                            </p>
                        </router-link>
                        <li class="nav-item has-treeview menu-close ">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-cogs"></i>
                            <p>
                                Setup
                                <i class="right fas fa-angle-left"></i>
                            </p>
                            </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <router-link to="/day_setup" class="nav-link">
                                    <i class="nav-icon fas fa-calendar-day"></i>
                                    <p>Day</p>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link to="/topic_type_setup" class="nav-link">
                                    <i class="nav-icon fas fa-file-powerpoint"></i>
                                    <p>Topic Type</p>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link to="/room_setup" class="nav-link">
                                    <i class="nav-icon fas fa-location-arrow"></i>
                                    <p>Room</p>
                                </router-link>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item has-treeview menu-close">
                    <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Users
                                <i class="right fas fa-angle-left"></i>
                            </p>
                            </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <router-link to="/barcamper" class="nav-link">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>Barcampers</p>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link to="/speaker" class="nav-link">
                                    <i class="nav-icon fas fa-user-ninja"></i>
                                    <p>Speakers</p>
                                </router-link>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item has-treeview menu-close">
                    <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Topic Board
                                <i class="right fas fa-angle-left"></i>
                            </p>
                            </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <router-link to="/topicboard1" class="nav-link">
                                    <i class="nav-icon fas fa-table"></i>
                                    <p>20 min Topic Board</p>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link to="/topicboard2" class="nav-link">
                                    <i class="nav-icon fas fa-table"></i>
                                    <p>45 min Topic Board</p>
                                </router-link>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item">
                        <router-link to="/populartrend" class="nav-link">
                            <i class="nav-icon fas fa-poll"></i>
                            <p>
                                Popular Trend
                                <!-- <span class="right badge badge-danger">New</span> -->
                            </p>
                        </router-link>
                    </li>

                    <li class="nav-item has-treeview menu-close">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-user-ninja"></i>
                            <p>
                                Speaker
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <router-link to="/speaker_rule" class="nav-link">
                                    <i class="nav-icon fas fa-user-ninja"></i>
                                    <p>Be Speaker</p>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link to="/mytopics" class="nav-link">
                                    <i class="nav-icon fas fa-user-ninja"></i>
                                    <p>My Topics</p>
                                </router-link>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item">
                        <router-link to="/votedhistory" class="nav-link">
                            <i class="nav-icon fas fa-history"></i>
                            <p>
                                Voted History
                                <!-- <span class="right badge badge-danger">New</span> -->
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                         <a class="nav-link" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            <i class="nav-icon fas fa-sign-out-alt"></i>
                            <p>
                                    {{ __('Logout') }}
                            </p>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                        </form>

                    </li>

                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <router-view></router-view>
                <vue-progress-bar></vue-progress-bar>
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <strong>Copyright &copy;2020 <a href="#">BCY</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 1.0.0
        </div>
    </footer>
</div>
<!-- ./wrapper -->

<script src="/js/app.js"></script>
</body>
</html>
