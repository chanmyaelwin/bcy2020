/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import { Form, HasError, AlertError } from 'vform'

import swal from 'sweetalert2'
window.swal = swal;

const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', swal.stopTimer)
      toast.addEventListener('mouseleave', swal.resumeTimer)
    }
})

window.toast = toast;

window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import vueProgressBar from 'vue-progressbar'
Vue.use(vueProgressBar,{
    color:'rgb(143,255,199)',
    failedColor:'red',
    height:'10px'
})

let routes = [
    {path: '/', component: require('./components/dashboard.vue').default},
    {path: '/profile', component: require('./components/profile.vue').default},
    {path: '/day_setup', component: require('./components/day_setup.vue').default},
    {path: '/topic_type_setup', component: require('./components/topic_type_setup.vue').default},
    {path: '/room_setup', component: require('./components/room_setup.vue').default},
    {path: '/barcamper', component: require('./components/barcamper.vue').default},
    {path: '/speaker_rule', component: require('./components/speaker_rule.vue').default},
    {path: '/speaker', component: require('./components/speaker.vue').default},
    {path: '/topics', component: require('./components/topics.vue').default},
    {path: '/dashboard', component: require('./components/dashboard.vue').default},
    {path: '/topicboard1', component: require('./components/topicboard1.vue').default},
    {path: '/topicboard2', component: require('./components/topicboard2.vue').default},
    {path: '/alltopics', component: require('./components/alltopics.vue').default},
    {path: '/populartrend', component: require('./components/populartrend.vue').default},
    {path: '/bespeaker', component: require('./components/bespeaker.vue').default},
    {path: '/mytopics', component: require('./components/mytopics.vue').default},
    {path: '/votedhistory', component: require('./components/votedhistory.vue').default}
]

const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
})


Vue.filter('uptext',function(text){
   return text.toUpperCase();
});

window.Fire = new Vue();

import moment from 'moment';
Vue.filter('myDate',function (created) {
    return moment(created).format("MMM Do YY");
});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    router
}).$mount('#app')

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

// import Select2Component
import Select2 from 'v-select2-component';

Vue.component('Select2', Select2);