<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic_Type extends Model
{
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'topic_type_name','duration','break_time'
    ];

    protected $table = 'topic_type';
}
