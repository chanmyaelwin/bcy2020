<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Trends;

class TrendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
        return Trends::latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'trend_name' => 'required|string|max:191',
            'trend_description' => 'required|string|max:191'
        ]);
        return Trends::create([
            'trend_name' => $request['trend_name'],
            'trend_description' => $request['trend_description'],
             'created_user_id' => auth()->user()->id,
             'point' => 0

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Trends::findOrFail($id);

        $this->validate($request,[
            'name'=>'required|string|max:191',
            'email'=>'required|email|string|max:191|unique:users,email,'.$user->id,
            'password'=> 'sometimes|min:6'
        ]);
        $user->update($request->all());   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Trends::findOrFail($id);
        $user->delete();
    }
}
