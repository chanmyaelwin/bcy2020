<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Topic_Type;

class TopicTypeController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth:api');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return Topic_Type::latest()->paginate(5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'topic_type_name' => 'required|string|max:191',
            'duration' => 'required|int|max:60|min:15',
            'break_time' => 'required|int|max:15|min:5'
        ]);
    
        return Topic_Type::create([
            'topic_type_name' => $request['topic_type_name'],
            'duration' => $request['duration'],
            'break_time' =>$request['break_time'],

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Topic_Type::findOrFail($id);

        $this->validate($request,[
            'topic_type_name' => 'required|string|max:191',
            'duration' => 'required|int|max:60|min:15',
            'break_time' => 'required|int|max:15|min:5'
        ]);
        $user->update($request->all());   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Topic_Type::findOrFail($id);
        $user->delete();
    }
}
