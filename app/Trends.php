<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trends extends Model
{

    protected $table = 'trends';
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'trend_name', 'trend_description','created_user_id','point'
    ];

}
