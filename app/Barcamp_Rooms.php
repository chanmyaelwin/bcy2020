<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barcamp_Rooms extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'room_name','available_person'
    ];

    protected $table = 'barcamp_rooms';
}
