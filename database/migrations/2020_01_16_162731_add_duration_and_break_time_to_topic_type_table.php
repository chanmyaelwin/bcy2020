<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDurationAndBreakTimeToTopicTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_type', function (Blueprint $table) {
            $table->string('duration',16)->after('topic_type_name');
            $table->string('break_time',16)->after('duration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_type', function (Blueprint $table) {
            $table->dropColumn('duration');
            $table->dropColumn('break_time');
        });
    }
}
